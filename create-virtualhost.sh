#!/bin/bash

echo -n "Enter name of host: "
read newHost

echo -n "Enter path from / to files of site (without / in the end): "
read newPath

#Add new record in hosts
file="/etc/hosts"
b=$(cat $file)
newContent="127.0.0.1 ${newHost}"$'\n'$b
sudo bash -c "echo '${newContent}' > $file"

#Add site in sites-available and add needed directives
sap=/etc/apache2/sites-available/$newHost.conf
sudo touch $sap
sudo chmod 777 $sap
directives="<VirtualHost *:80>
        ServerName ${newHost}
        DocumentRoot ${newPath}/public/
        ErrorLog /var/log/apache2/${newHost}.log
        <Directory ${newPath}/public/>
		DirectoryIndex index.php
		AllowOverride All
		Order allow,deny
		Allow from all
        </Directory>
</VirtualHost>"
echo "$directives">$sap

#Enable virtual host
sudo a2ensite $newHost.conf

#Enable rewrite
sudo a2enmod rewrite

#Restarting apache2
sudo /usr/sbin/apache2ctl restart
