# Exchange operations

## Installation using Composer

1. Clone project via git, then run:

```bash
$ composer install
```
When ypu will see the questions in your cli - just press Enter button

2.1. Once installed, you can test it out immediately using PHP's built-in web server:

```bash
$ cd path/to/install
$ php -S 0.0.0.0:8080 -t public/ public/index.php
# OR use the composer alias:
$ composer run --timeout 0 serve
```

This will start the cli-server on port 8080, and bind it to all network
interfaces. You can then visit the site at http://localhost:8080/ - which will bring up project home page.

2.2. Or create your own virtual host via Apache, just running, and follow instructions:

```bash
$ cd path/to/install
$ sudo bash create-virtualhost.sh
```

This will create virtual host on your PC.
You can then visit the site at http://your-site-name:8080/ - which will bring up project home page.

3. Project needed php-apcu package, to be installed, so you will needed to run in cli: 

```bash
$ sudo apt-get install php-apcu
```