<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace ExchangeTest\Model;

use Exchange\Model\History;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class HistoryTest extends AbstractHttpControllerTestCase
{
    public function testHistoryInitialState()
    {
        $history = new History();

        $this->assertNull($history->id, '"id" should initially be null');
        $this->assertNull($history->amountGiven, '"amountGiven" should initially be null');
        $this->assertNull($history->amountDesired, '"amountDesired" should initially be null');
        $this->assertNull($history->currencyGiven, '"currencyGiven" should initially be null');
        $this->assertNull($history->currencyDesired, '"currencyDesired" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $history = new History();
        $data  = [
            'id'     => 1,
            'amount_given' => 10,
            'amount_desired'  => 20,
            'currency_given' => 'UAH',
            'currency_desired'  => 'RUB',
        ];

        $history->exchangeArray($data);

        $this->assertSame($data['id'], $history->id,'"id" was not set correctly');
        $this->assertSame($data['amount_given'], $history->amountGiven, '"amountGiven" was not set correctly');
        $this->assertSame($data['amount_desired'], $history->amountDesired, '"amountDesired" was not set correctly');
        $this->assertSame($data['currency_given'], $history->currencyGiven, '"currencyGiven" was not set correctly');
        $this->assertSame($data['currency_desired'], $history->currencyDesired, '"currencyDesired" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $history = new History();

        $history->exchangeArray([
            'id'     => 123,
            'amount_given' => 111,
            'amount_desired'  => 222,
            'currency_given' => 'currency1',
            'currency_desired'  => 'currency2',
        ]);
        $history->exchangeArray(array());

        $this->assertNull($history->id, '"id" should have defaulted to null');
        $this->assertNull($history->amountGiven, '"amountGiven" should have defaulted to null');
        $this->assertNull($history->amountDesired, '"amountDesired" should have defaulted to null');
        $this->assertNull($history->currencyGiven, '"currencyGiven" should have defaulted to null');
        $this->assertNull($history->currencyDesired, '"currencyDesired" should have defaulted to null');
    }
}
