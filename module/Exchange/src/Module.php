<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Exchange;

use Exchange\Controller\HistoryController;
use Exchange\Model\History;
use Exchange\Model\HistoryTable;
use Exchange\Model\HistoryTableGateway;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    const VERSION = '3.0.3-dev';

    public function getServiceConfig()
    {
        return [
            'factories' => [
                HistoryTable::class => function ($container) {
                    $tableGateway = $container->get(HistoryTableGateway::class);
                    return new HistoryTable($tableGateway);
                },
                HistoryTableGateway::class => function ($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new History());
                    return new TableGateway('history', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [
                HistoryController::class => function ($container) {
                    return new HistoryController(
                        $container->get(HistoryTable::class)
                    );
                },
            ],
        ];
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}

