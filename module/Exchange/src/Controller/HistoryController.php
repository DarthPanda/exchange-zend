<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Exchange\Controller;

use Exchange\Model\History;
use Exchange\Model\HistoryTable;
use Exchange\Form\HistoryForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class HistoryController extends AbstractActionController
{
    /**
     * @var HistoryTable
     */
    private $table;

    /**
     * HistoryController constructor.
     * @param HistoryTable $table
     */
    public function __construct(HistoryTable $table)
    {
        $this->table = $table;
    }

    /**
     * @return array|JsonModel
     */
    public function addAction()
    {
        $form = new HistoryForm();
        $history = new History();

        $form->get('submit')->setValue('Exchange');
        $request = $this->getRequest();


        if (!$request->isPost()) {
            return [
                'form' => $form,
                'histories' => $this->table->fetchAll(),
            ];
        }

        $form->setInputFilter($history->getInputFilter());

        $form->setData($request->getPost());
        if (!$form->isValid()) {
            return [
                'form' => $form,
                'histories' => $this->table->fetchAll(),
            ];
        }

        $history->exchangeArray($form->getData());
        $this->table->saveHistory($history);

        return new JsonModel($form->getData());
    }

    /**
     * @return JsonModel
     */
    public function getCurrencyRateAction()
    {
        $currencies = History::getCurrencies();

        return new JsonModel($currencies['rates']);
    }
}
