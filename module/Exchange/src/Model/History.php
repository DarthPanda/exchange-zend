<?php
namespace Exchange\Model;

use DomainException;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\StringLength;

class History implements InputFilterAwareInterface
{
    public $id;
    public $amountGiven;
    public $amountDesired;
    public $currencyGiven;
    public $currencyDesired;
    private $inputFilter;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id     = (isset($data['id'])) ? $data['id'] : null;
        $this->amountGiven = (isset($data['amount_given'])) ? $data['amount_given'] : null;
        $this->amountDesired  = (isset($data['amount_desired'])) ? $data['amount_desired'] : null;
        $this->currencyGiven     = (isset($data['currency_given'])) ? $data['currency_given'] : null;
        $this->currencyDesired = (isset($data['currency_desired'])) ? $data['currency_desired'] : null;
    }

    /**
     * @param InputFilterInterface $inputFilter
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new DomainException(sprintf(
            '%s does not allow injection of an alternate input filter',
            __CLASS__
        ));
    }

    /**
     * @return InputFilter
     */
    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }

        $inputFilter = new InputFilter();

        $inputFilter->add([
            'name' => 'id',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);

        $inputFilter->add([
            'name' => 'amount_given',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);
        $inputFilter->add([
            'name' => 'amount_desired',
            'required' => true,
            'filters' => [
                ['name' => ToInt::class],
            ],
        ]);
        $inputFilter->add([
            'name' => 'currency_given',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $inputFilter->add([
            'name' => 'currency_desired',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $this->inputFilter = $inputFilter;
        return $this->inputFilter;
    }

    /**
     * @return array|mixed
     */
    public function getExchangeAPI()
    {
        $apcKeyExist = apcu_exists('currencies');

        if (!$apcKeyExist) {
            $url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json";

            $content = file_get_contents($url);
            $jsonResult = json_decode($content);
            $jsonResult = array_slice($jsonResult, 0, 20);
            apcu_add('currencies', $jsonResult, 3600);

        } else {
            $jsonResult = apcu_fetch('currencies');
        }


        return $jsonResult;
    }

    /**
     * @return array
     */
    public static function getCurrencies()
    {
        $currencies = self::getExchangeAPI();
        $newArray = [];

        foreach ($currencies as $key => $currency)
        {
            $newArray['currencies'][$currency->cc] = $currency->cc;
            $newArray['rates'][$currency->cc] = $currency->rate;
        }

        return $newArray;
    }
}