<?php

namespace Exchange\Model;

use RuntimeException;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;

class HistoryTable
{

    private $tableGateway;

    /**
     * HistoryTable constructor.
     * @param TableGatewayInterface $tableGateway
     */
    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return mixed
     */
    public function fetchAll()
    {
        return $this->tableGateway->select(function (Select $select){
            $select
                ->order('id DESC')
                ->limit(5)
            ;
        });
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getHistory($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    /**
     * @param History $history
     */
    public function saveHistory(History $history)
    {
        $data = [
            'amount_given' => $history->amountGiven,
            'amount_desired' => $history->amountDesired,
            'currency_given' => $history->currencyGiven,
            'currency_desired' => $history->currencyDesired,
        ];

        $id = (int)$history->id;

        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }

        if (!$this->getHistory($id)) {
            throw new RuntimeException(sprintf(
                'Cannot update history with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    /**
     * @param $id
     */
    public function deleteHistory($id)
    {
        $this->tableGateway->delete(['id' => (int)$id]);
    }
}