<?php
namespace Exchange\Form;

use Exchange\Model\History;
use Zend\Form\Form;

class HistoryForm extends Form
{
    /**
     * HistoryForm constructor.
     * @param null $name
     */
    public function __construct($name = null)
    {
        parent::__construct('history');


        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'amount_given',
            'type' => 'number',
            'attributes' => [
                'class' => 'col-xs-6 form-control',
                'id' => 'amount-given',
            ],
            'options' => [
                'label' => 'Amount given',
            ],
        ]);
        $this->add([
            'name' => 'amount_desired',
            'type' => 'number',
            'attributes' => [
                'class' => 'col-xs-6 form-control',
                'id' => 'amount-desired',
            ],
            'options' => [
                'label' => 'Amount desired',
            ],
        ]);
        $this->add([
            'name' => 'currency_given',
            'type' => 'select',
            'attributes' => [
                'class' => 'col-xs-6  form-control',
                'id' => 'currency-given',
            ],
            'options' => [
                'label' => 'currency given',
                'value_options' => History::getCurrencies()['currencies'],
            ],
        ]);
        $this->add([
            'name' => 'currency_desired',
            'type' => 'select',
            'attributes' => [
                'class' => 'col-xs-6 form-control',
                'id' => 'currency-desired',
            ],
            'options' => [
                'label' => 'currency desired',
                'value_options' => History::getCurrencies()['currencies'],
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'class' => 'center-block',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}