CREATE TABLE history (id INTEGER PRIMARY KEY AUTOINCREMENT,amount_given int(10) NOT NULL,amount_desired int(10) NOT NULL,currency_given varchar(100) NOT NULL,currency_desired varchar(100) NOT NULL);
INSERT INTO history (amount_given, amount_desired, currency_given, currency_desired)
VALUES (11, 222, 'uah', 'usd');
INSERT INTO history (amount_given, amount_desired, currency_given, currency_desired)
VALUES (22, 333, 'uah', 'usd');
INSERT INTO history (amount_given, amount_desired, currency_given, currency_desired)
VALUES (33, 444, 'uah', 'usd');
INSERT INTO history (amount_given, amount_desired, currency_given,currency_desired)
VALUES (44, 555, 'uah', 'eur');